import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tatsam/model/country.dart';
import 'package:tatsam/providers/country.dart';
import 'package:tatsam/util/constants.dart';

class ApiService {
  String apiUrl = Constants().baseUrl;

  getPosts(CountryNotifier countryNotifier, int offset) async {
    List<Country> countryList = [];
    http.get(apiUrl + "$offset").then((response) {
      print('Response status: ${response.statusCode}');
      Map fetchedCountryList = jsonDecode(response.body);
      Map<String, dynamic> fetchedDataField = fetchedCountryList['data'];
      // Empty map to arrange the data field of the fetched api
      Map newSubMap = {};
      fetchedDataField.forEach((k, v) => {
            newSubMap = {},
            newSubMap.addAll({"countryCode": k.toString()}),
            newSubMap.addAll(v),
            countryList.add(Country.fromJson(newSubMap))
          });
      print(countryList.length);

      countryNotifier.setCountryList(countryList);
    });
  }
}
