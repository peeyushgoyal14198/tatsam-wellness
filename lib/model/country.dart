class Country {
  String region;
  String countryName;
  String countryCode;
  Country({this.region, this.countryCode, this.countryName});

  factory Country.fromJson(Map<dynamic, dynamic> json) {
    return Country(
      region: json['region'],
      countryName: json['country'],
      countryCode: json['countryCode'],
    );
  }
  factory Country.fromLocalJson(Map<dynamic, dynamic> json) {
    return Country(
      region: json['region'],
      countryName: json['countryName'],
      countryCode: json['countryCode'],
    );
  }
  Map<String, String> toJson() => {
        'countryCode': countryCode,
        'countryName': countryName,
        'region': region
      };
}
