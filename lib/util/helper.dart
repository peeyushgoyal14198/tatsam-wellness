import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class Helper {
  void navigateTo(
    BuildContext context,
    Widget navigateTo,
  ) {
    Navigator.push(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeft,
        child: navigateTo,
      ),
    );
  }
}
