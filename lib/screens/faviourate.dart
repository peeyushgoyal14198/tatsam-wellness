import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tatsam/backend/faviourate.dart';
import 'package:tatsam/model/country.dart';
import 'package:tatsam/providers/country.dart';
import 'package:tatsam/widgets/country_cards.dart';
import 'package:tatsam/widgets/loader.dart';
import 'package:tatsam/widgets/slide_animation.dart';

class FaviourateScreen extends StatefulWidget {
  FaviourateScreen({Key key}) : super(key: key);

  @override
  _FaviourateScreenState createState() => _FaviourateScreenState();
}

class _FaviourateScreenState extends State<FaviourateScreen> {
  ScrollController scrollController = new ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorites"),
      ),
      body: FutureBuilder(
        future: FavoriteDatabase().listAll(),
        builder: (context, snap) {
          if (!snap.hasData) {
            return LoaderWidget();
          } else if (snap.data.length == 0) {
            return Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(
                "assets/images/articleNotFound.png",
                fit: BoxFit.cover,
              ),
            );
          } else {
            return SlideAnimationWidget(
              scrollController: scrollController,
              widget: List.generate(snap.data.length, (index) {
                Map map = snap.data[index];
                Country country = Country.fromLocalJson(map);
                return CountryCardWidget(
                  faviourateList:
                      Provider.of<CountryNotifier>(context, listen: false)
                          .faviourateList,
                  country: country,
                  showIcon: false,
                );
              }),
            );
          }
        },
      ),
    );
  }
}
