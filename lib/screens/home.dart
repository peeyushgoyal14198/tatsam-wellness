import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tatsam/backend/faviourate.dart';
import 'package:tatsam/providers/country.dart';
import 'package:tatsam/screens/faviourate.dart';
import 'package:tatsam/services/api.dart';
import 'package:tatsam/util/helper.dart';
import 'package:tatsam/widgets/country_cards.dart';
import 'package:tatsam/widgets/loader.dart';
import 'package:tatsam/widgets/slide_animation.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool hasInternet = true;
  final Connectivity connectivity = Connectivity();
  var localFaviourateDatabse = FavoriteDatabase();
  ScrollController scrollController  = new ScrollController();
  List faviourateList = [];
  bool loader = true;
  int offset = 10;
  bool paginatedLoader = false;
  StreamSubscription<ConnectivityResult> connectivitySubscription;
  @override
  void initState() {
    initScrollListeners();
    initConnectivity();
    connectivitySubscription =
        connectivity.onConnectivityChanged.listen(updateConnectionStatus);
    getLocalStorageData();
    if (hasInternet) {
      fetchData(offset);
    }
    super.initState();
  }

  @override
  void dispose() {
    connectivitySubscription.cancel();
    super.dispose();
  }

  // Intializing scroll listeners for pagination
  void initScrollListeners() {
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        print("I am here");
        setState(() {
          offset += 10;
        });
        fetchData(offset);
      }
    });
  }

  // Fetching data from internet
  void fetchData(int givenOffset) {
    CountryNotifier countryNotifier =
        Provider.of<CountryNotifier>(context, listen: false);
    countryNotifier.setPaginatedLoader(true);
    ApiService().getPosts(countryNotifier, givenOffset);
  }

  // Fetching data from local storage
  Future<void> getLocalStorageData() async {
    var list = await localFaviourateDatabse.listAll();
    Provider.of<CountryNotifier>(context, listen: false)
        .setFaviourateList(list ?? []);
    setState(() {
      faviourateList = list ?? [];
      loader = false;
    });
  }

  // Initializing the internet connectivity
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return updateConnectionStatus(result);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text("Home Screen"),
          actions: [
            IconButton(
              icon: Icon(Icons.favorite),
              color: Colors.red,
              onPressed: () {
                Helper().navigateTo(context, FaviourateScreen());
              },
            )
          ],
        ),
        body: loader
            ? Center(
                child: LoaderWidget(),
              )
            : buildHomeWidget());
  }

  Widget buildHomeWidget() {
    return Consumer<CountryNotifier>(builder:
        (BuildContext context, CountryNotifier countryNotifier, Widget child) {
      faviourateList = countryNotifier.faviourateList;
      paginatedLoader = countryNotifier.paginatedLoader;
      if (!hasInternet) {
        print("In No Internet");
        return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Image.asset(
            "assets/images/noInternet.png",
            fit: BoxFit.cover,
          ),
        );
      } else if (countryNotifier.countryList != null) {
        return Stack(
          children: [
            ListView(
              controller: scrollController,
              children:
                  List.generate(countryNotifier.countryList.length, (index) {
                return CountryCardWidget(
                  country: countryNotifier.countryList[index],
                  faviourateList: faviourateList,
                );
              }),
            ),
            Visibility(
              visible: paginatedLoader,
              child: LoaderWidget(
                backGroundColor: Colors.transparent,
              ),
            )
          ],
        );
      } else {
        print("Loading");
        return Center(
          child: LoaderWidget(),
        );
      }
    });
  }

  Future<void> updateConnectionStatus(ConnectivityResult result) async {
    // Updating the state when user has no internet access
    print("Reached Here");
    if (result == ConnectivityResult.none) {
      setState(() => hasInternet = false);
    } else if (result == ConnectivityResult.wifi) {
      setState(() => hasInternet = true);
      fetchData(offset);
    } else if (result == ConnectivityResult.mobile) {
      setState(() => hasInternet = true);
      fetchData(offset);
    }
  }
}
