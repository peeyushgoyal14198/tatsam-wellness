import 'package:flutter/material.dart';
import 'package:tatsam/backend/faviourate.dart';
import 'package:tatsam/model/country.dart';

class CountryNotifier with ChangeNotifier {
  List<Country> countryList;
  bool paginatedLoader = false;
  List faviourateList = [];

  // Setting country list as in result from the api
  setCountryList(List<Country> list) {
    if (countryList == null) {
      countryList = [];
    }
    countryList += removeDuplicatesIfPresent(list, countryList);
    setPaginatedLoader(false);
    notifyListeners();
  }

  // Getting country list which came from api
  List<Country> getCountryList() {
    return countryList;
  }

  // Add data to local storage for offline access
  addCountryData(Map item) async {
    await FavoriteDatabase().add(item);
    faviourateList.add(item);
    notifyListeners();
  }

  // Remove data from local storage
  removeCountryData(Map item) async {
    await FavoriteDatabase().remove(item);
    int index = getItemIndex(item);
    faviourateList.removeAt(index);
    notifyListeners();
  }

  // For setting faviourate list
  setFaviourateList(List list) {
    faviourateList = list;
    notifyListeners();
  }

  // Getting index of the item to be removed from the faviourate local storage
  int getItemIndex(Map item) {
    int index = 0;
    for (int i = 0; i < faviourateList.length; i++) {
      if (item['countryCode'] == faviourateList[i]['countryCode']) {
        index = i;
      }
    }
    return index;
  }

  // Set value of paginated loader when pagination occurs
  setPaginatedLoader(bool value) {
    paginatedLoader = value;
    notifyListeners();
  }
  
  // To avoid duplicates in our original list
  List<Country> removeDuplicatesIfPresent(List newApiList, List actualCountryList) {
    List<Country> newReturnList = [];
    for (int i = 0; i < newApiList.length; i++) {
      Country newCountry = newApiList[i];
      bool isPresent = false;
      for (int j = 0; j < actualCountryList.length; j++) {
        Country oldCountry = actualCountryList[j];
        if (oldCountry.countryCode == newCountry.countryCode) {
          isPresent = true;
        }
      }
      if (!isPresent) {
        newReturnList.add(newCountry);
      }
    }
    return newReturnList;
  }
}
