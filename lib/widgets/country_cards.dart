import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tatsam/model/country.dart';
import 'package:tatsam/providers/country.dart';

class CountryCardWidget extends StatefulWidget {
  final Country country;
  final List faviourateList;
  bool showIcon;

  CountryCardWidget({
    Key key,
    @required this.country,
    @required this.faviourateList,this.showIcon = true
  }) : super(key: key);

  @override
  _CountryCardWidgetState createState() => _CountryCardWidgetState();
}

class _CountryCardWidgetState extends State<CountryCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Card(
        elevation: 5,
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      widget.country.countryCode,
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    Spacer(),
                    widget.showIcon ? 
                    IconButton(
                      icon: Icon(Icons.favorite),
                      color: checkIfValuePresent() ? Colors.red : Colors.grey,
                      onPressed: () {
                        print(widget.country.toJson());
                        if (checkIfValuePresent()) {
                          Provider.of<CountryNotifier>(context, listen: false)
                              .removeCountryData(widget.country.toJson());
                        } else {
                          Provider.of<CountryNotifier>(context, listen: false)
                              .addCountryData(widget.country.toJson());
                        }
                      },
                    ) : Container()
                  ],
                ),
                Container(
                  height: 10,
                ),
                Text(
                  widget.country.countryName,
                  style: Theme.of(context).textTheme.headline4,
                ),
                Text(
                  widget.country.region,
                  style: Theme.of(context).textTheme.headline4,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool checkIfValuePresent() {
    bool isPresent = false;
    for (int i = 0; i < widget.faviourateList.length; i++) {
      Map map = widget.faviourateList[i];
      if (map != null) {
        if (map['countryCode'] == widget.country.countryCode) {
          isPresent = true;
          break;
        }
      }
    }
    return isPresent;
  }
}
