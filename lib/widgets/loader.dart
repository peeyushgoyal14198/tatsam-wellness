import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

// A common custom loader
class LoaderWidget extends StatefulWidget {
  final Color backGroundColor;
  LoaderWidget({Key key, this.backGroundColor = Colors.white})
      : super(key: key);

  @override
  _LoaderWidgetState createState() => _LoaderWidgetState();
}

class _LoaderWidgetState extends State<LoaderWidget>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.backGroundColor,
      child: Center(
        child: SpinKitFadingCircle(
          size: 50,
          color: Theme.of(context).primaryColor,
          controller: AnimationController(
              vsync: this, duration: const Duration(milliseconds: 500)),
        ),
      ),
    );
  }
}
