import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class SlideAnimationWidget extends StatelessWidget {
  final List<Widget> widget;
  final ScrollController scrollController;
  const SlideAnimationWidget(
      {Key key, @required this.widget, @required this.scrollController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AnimationLimiter(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: AnimationConfiguration.toStaggeredList(
            duration: const Duration(milliseconds: 600),
            childAnimationBuilder: (widget) => SlideAnimation(
                  horizontalOffset: 10.0,
                  child: FadeInAnimation(
                    child: widget,
                  ),
                ),
            children: widget),
      )),
    );
  }
}
